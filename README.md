# Tennis Match

Code test that keeps score of a tennis game

## Install packages

```
npn install
```

## Getting Started

Enter node shell, then run:

```
var models = require('./models')
var match = new models.Match("kyrgios", "tomic")
```

## When a player wins a point

```
match.pointWonBy(match.playerOne)
```

## To check a match score

```
match.score()
```

## To run tests

```
npm run test
```
