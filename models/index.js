class Player {
  constructor(name) {
    this.name = name
    this.setsWon = 0
    this.gamesWon = 0
    this.pointsWon = 0
  }
}

class Match {
  constructor(playerOne, playerTwo) {
    this.playerOne = new Player(playerOne)
    this.playerTwo = new Player(playerTwo)
    this.scoreMap = {
      0: "love",
      1: "15",
      2: "30",
      3: "40"
    }
  }

  startGame() {
    this.playerOne.pointsWon = 0
    this.playerTwo.pointsWon = 0
  }

  startSet() {
    this.playerOne.gamesWon = 0
    this.playerTwo.gamesWon = 0
  }

  getOtherPlayer(player) {
    if (this.playerOne.name !== player.name) {
      return this.playerOne
    } else {
      return this.playerTwo
    }
  }

  checkScore(pointWinner, otherPlayer, minPoints, winBy) {
    if (pointWinner >= minPoints) {
      var pointsDiff = pointWinner - otherPlayer
      if (pointsDiff >= winBy) {
        return true
      }
      return false
    }
    return false
  }

  gameScore() {
    var playerOneScore = this.scoreMap[this.playerOne.pointsWon]
    var playerTwoScore = this.scoreMap[this.playerTwo.pointsWon]
    if (playerOneScore && playerTwoScore) {
      return `${playerOneScore}-${playerTwoScore}`
    }
    if (this.playerOne.pointsWon == this.playerTwo.pointsWon) {
      return "Deuce"
    } else {
      return "Advantage"
    }
  }

  score() {
    if (this.playerOne.setsWon == 1) {
      return `${this.playerOne.name} wins`
    }
    if (this.playerTwo.setsWon == 1) {
      return `${this.playerTwo.name} wins`
    }
    return `${this.playerOne.gamesWon}-${this.playerTwo.gamesWon} ${this.gameScore()}`
  }

  pointWonBy(pointWinner) {
    pointWinner.pointsWon ++
    var pointLoser = this.getOtherPlayer(pointWinner)
    var gameWon = this.checkScore(pointWinner.pointsWon, pointLoser.pointsWon, 4, 2)
    var score = this.score()

    if (gameWon) {
      pointWinner.gamesWon ++
      this.startGame()
      var setWon = this.checkScore(pointWinner.gamesWon, pointLoser.gamesWon, 6, 2)

      if (setWon) {
        pointWinner.setsWon ++
        this.startSet()
      }
    }
  }
}

module.exports.Match = Match
