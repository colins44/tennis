var assert = require('assert');
var models = require('../models')

describe('[Tennis]', function() {
  describe('Test Match', function() {
    it('match should start', function() {
      let match = new models.Match("bernard", "nick")
      assert.equal(match.score(), '0-0 love-love');
    });

    it('point won', function() {
      let match = new models.Match("bernard", "nick")
      match.pointWonBy(match.playerOne)
      assert.equal(match.score(), '0-0 15-love');
    });

    it('deuce', function() {
      let match = new models.Match("bernard", "nick")
      match.playerOne.pointsWon = 4
      match.playerTwo.pointsWon = 4
      assert.equal(match.score(), '0-0 Deuce');
    });

    it('advantage', function() {
      let match = new models.Match("bernard", "nick")
      match.playerOne.pointsWon = 4
      match.playerTwo.pointsWon = 5
      assert.equal(match.score(), '0-0 Advantage');
    });

    it('win game', function() {
      let match = new models.Match("bernard", "nick")
      match.playerOne.pointsWon = 4
      match.playerTwo.pointsWon = 5
      match.pointWonBy(match.playerTwo)
      assert.equal(match.score(), '0-1 love-love');
    });

    it('win set', function() {
      let match = new models.Match("bernard", "nick")
      match.playerOne.gamesWon = 4
      match.playerOne.pointsWon = 0
      match.playerTwo.gamesWon = 5
      match.playerTwo.pointsWon = 3
      match.pointWonBy(match.playerTwo)
      assert.equal(match.score(), 'nick wins');
    });

  });
});
